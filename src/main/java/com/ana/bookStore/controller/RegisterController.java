package com.ana.bookStore.controller;

import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegisterController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    LoginHolder loginHolder;

   @RequestMapping(value = "/register", method =  RequestMethod.POST)
   public String saveUser(@RequestParam ("username") String username,
                          @RequestParam("emailAddress") String email,
                          @RequestParam("password") String password,
                          @RequestParam("confirmPassword") String confirmPassword,
                          @RequestParam("address") String address) throws Exception {
       if(username == null || email == null || password == null || confirmPassword == null || address == null) {
           throw new Exception("All fields are required!");
       }
       if(!password.equals(confirmPassword) || userRepository.findByEmailAddress(email) != null) {
           throw new Exception("Passwords don't match!");
       }

       User user = new User(username, email, password, address);
       userRepository.save(user);
       User currentUser = userRepository.findByEmailAddress(email);
       loginHolder.setCurrentUserId(currentUser.getId());
          return "redirect:/homepage";
       }
}



