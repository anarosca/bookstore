package com.ana.bookStore.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(Exception ex) {
        ModelAndView mav = new ModelAndView("error.html");
        mav.addObject("exception", ex.getMessage());
        return mav;
    }
    @ExceptionHandler(NumberFormatException.class)
    public ModelAndView handleError(NumberFormatException ex) {
        ModelAndView mav = new ModelAndView("error.html");
        mav.addObject("exception", ex.getMessage());
        return mav;
    }
    @ExceptionHandler(NullPointerException.class)
    public ModelAndView handleError(NullPointerException ex) {
        ModelAndView mav = new ModelAndView("error.html");
        mav.addObject("exception", ex.getMessage());
        return mav;
    }
}
