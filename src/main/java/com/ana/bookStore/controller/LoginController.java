package com.ana.bookStore.controller;


import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    LoginHolder loginHolder;

    @RequestMapping(value="/login", method= RequestMethod.GET )
    public String login(@RequestParam("email") String email, @RequestParam("password") String password) throws Exception {
        User user = userRepository.findByEmailAddress(email);
        if(user == null || !user.getPassword().equals(password))
            throw new Exception("Username or password is incorrect. Please try again!");
        loginHolder.setCurrentUserId(user.getId());
        return "redirect:/homepage";
    }

    @RequestMapping(value="/logout", method= RequestMethod.GET )
    public String logout() {
        loginHolder.setCurrentUserId(0L);
        return "redirect:/homepage";
    }
}
