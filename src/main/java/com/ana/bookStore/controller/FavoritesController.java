package com.ana.bookStore.controller;

import com.ana.bookStore.*;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class FavoritesController {

    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    BookService bookService;
    @Autowired
    UserService userService;
    @Autowired
    LoginHolder loginHolder;

    @RequestMapping("/addToFavorites")
    public String addToFavorites(@RequestParam("id") Long id) {
        if(loginHolder.getCurrentUserId() == 0){
            return "redirect:/signin.html";
        }
        List<UserBook> favoritesItems = userBookRepository.findAllByUserAndActions(
                userService.findById(loginHolder.getCurrentUserId()).get(),
                Actions.ADD_TO_FAVORITES);

        for(UserBook item: favoritesItems){
            if(item.getBook().getId() == id){
                return "redirect:/favorites"; //if book is already in your favorites list it should redirect you back to /favorites
            }
        }
        Book book = bookService.findById(id).get();
        User user = userService.findById(loginHolder.getCurrentUserId()).get();
        UserBook favoritesItem = new UserBook(user,book);
        favoritesItem.setActions(Actions.ADD_TO_FAVORITES);
        book.getUsers().add(favoritesItem);
        user.getBooks().add(favoritesItem);
        userBookRepository.save(favoritesItem);
        return "redirect:/favorites";
    }
    @RequestMapping(value = "/favorites", method= RequestMethod.GET)
    public Object favoritesList(){
        if(loginHolder.getCurrentUserId() == 0){
            return "redirect:/signin.html";
        }
        List<UserBook> favoritesItems = userBookRepository.findAllByUserAndActions(
                userService.findById(loginHolder.getCurrentUserId()).get(),
                Actions.ADD_TO_FAVORITES);

        List<Book> books = favoritesItems
                .stream()
                .map(item -> item.getBook())
                .collect(Collectors.toList());

        ModelAndView modelAndView = new ModelAndView("favorites.html");
        modelAndView.addObject("books",books);
        modelAndView.addObject("totalItems",books.size());
        modelAndView.addObject("categories", BookService.categories);
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);

        return modelAndView;
    }
    @RequestMapping(value = "/delete", method = RequestMethod.POST )
    public String delete(@RequestParam("id") Long id){
        userBookRepository.deleteByUserAndBookAndActions(
                userService.findById(loginHolder.getCurrentUserId()).get(),
                bookService.findById(id).get(),
                Actions.ADD_TO_FAVORITES);
        return "redirect:/favorites";
    }

}
