package com.ana.bookStore.controller;

import com.ana.bookStore.*;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.Review;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.repository.ReviewRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BookController {


    @Autowired
    private LoginHolder loginHolder;
    @Autowired
    private BookService bookService;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private UserRepository userRepository;

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @RequestMapping("/homepage")
    public ModelAndView homepage(
        @RequestParam("page") Optional<Integer> page,
        @RequestParam("size") Optional<Integer> size) throws IOException, InterruptedException {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(15);
        List<Book> books = bookService.findAllByCategory("Best Seller");
        List<UserBook> userBooks = new ArrayList<>();

        if(books.size() == 0) {
        bookService.getBooksInfo();
        books = bookService.findAllByCategory("Best Seller");
    }
            ModelAndView modelAndView = new ModelAndView("index.html");

            Page<Book> bookPage = bookService.findPaginated(PageRequest.of(currentPage - 1, pageSize), books);
            modelAndView.addObject("bookPage", bookPage);

            int totalPages = bookPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                modelAndView.addObject("pageNumbers", pageNumbers);
            }
        modelAndView.addObject("categories", bookService.categories);
        modelAndView.addObject("currentPage", page);
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);
        modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));
        return modelAndView;
    }

    @RequestMapping("/book")
    public ModelAndView viewBook(@RequestParam("book_id") Long bookId) throws Exception {
        Book book = bookService.findById(bookId).get();
        ModelAndView modelAndView = new ModelAndView("book");
        List<Book> booksByCategory = bookService.findAllByCategory(book.getCategory());
        List<Book> relatedSearch = bookService.getRelatedSearchItems(booksByCategory);
        List<Review> reviews = reviewRepository.findAllByBookId(bookId)
                .stream()
                .filter(review -> review.getDate() != null)
                .collect(Collectors.toList());
        Map<String, String> ratings = bookService.calculateRatingsPerBook(bookId);
        modelAndView.addObject("totalRatings", ratings.get("Total Ratings"));
        modelAndView.addObject("excellentRating", ratings.get("Excellent Rating"));
        modelAndView.addObject("goodRating", ratings.get("Good Rating"));
        modelAndView.addObject("mediocreRating", ratings.get("Mediocre Rating"));
        modelAndView.addObject("poorRating", ratings.get("Poor Rating"));
        modelAndView.addObject("book", book);
        modelAndView.addObject("categories", BookService.categories);
        modelAndView.addObject("relatedSearchItems", relatedSearch);
        modelAndView.addObject("reviews", reviews);
        modelAndView.addObject("logged1",loginHolder.getCurrentUserId() > 0);
        modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));

        return modelAndView;
    }
    @RequestMapping(value = "/rating", method = RequestMethod.POST)
    public String addBookRating(@RequestParam ("name") String name,
                           @RequestParam("id") Long bookId,
                           @RequestParam("rating") String rating) throws Exception {

        if(loginHolder.getCurrentUserId() == 0){
            return "redirect:/signin.html";
        }
        bookService.addBookRating(name, bookId, loginHolder.getCurrentUserId(), rating);
        return "redirect:/book?book_id=" + bookId;


    }
    @RequestMapping(value = "/review", method = RequestMethod.POST)
    public String addReview(@RequestParam("id") Long bookId, @RequestParam("review") String text) throws Exception {
        if(loginHolder.getCurrentUserId() == 0){
            return "redirect:/signin.html";
        }
        bookService.addBookReview(bookId, text);
        return "redirect:/book?book_id=" + bookId;
    }

    @RequestMapping("/categories")
    public ModelAndView categories(@RequestParam("category") String category,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(16);

        ModelAndView modelAndView = new ModelAndView("categories.html");

        List<Book> booksByCategory = bookService.findAllByCategory(category);
        Page<Book> bookPage = bookService.findPaginated(PageRequest.of(currentPage - 1, pageSize), booksByCategory);
        modelAndView.addObject("bookPage", bookPage);

        int totalPages = bookPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            modelAndView.addObject("pageNumbers", pageNumbers);
        }

        modelAndView.addObject("logged2", loginHolder.getCurrentUserId() > 0);
        modelAndView.addObject("category", category);
        modelAndView.addObject("categories", BookService.categories);
        return modelAndView;
    }

    @RequestMapping("/search")
    public String search(@RequestParam("search") String bookToSearch) throws Exception {

            Optional<Book> book = bookService.findByBookTitleIgnoreCase(bookToSearch);
            if(!book.isPresent())
                throw new Exception("Sorry, we couldn't find your book..");
            return "redirect:/book?book_id=" + book.get().getId();


    }

}
