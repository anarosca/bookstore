package com.ana.bookStore.controller;

import com.ana.bookStore.*;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class ShoppingCartController {
    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LoginHolder loginHolder;
    @Autowired
    ShoppingCartService shoppingCartService;

    @RequestMapping(value = "/addToCart", method = RequestMethod.POST)
    public String addToCart(@RequestParam("id") Long bookId) throws Exception {
        if (loginHolder.getCurrentUserId() == 0) {
            return "redirect:/signin.html";
        }
        shoppingCartService.addToCart(loginHolder.getCurrentUserId(), bookId);
        return "redirect:/shopping_cart";
    }

    @GetMapping("/shopping_cart")
    public Object shoppingCartList() {
        if (loginHolder.getCurrentUserId() == 0) {
            return "redirect:/signin.html";
        }
        List<Book> books = shoppingCartService.shoppingCartList(loginHolder.getCurrentUserId());

        ModelAndView modelAndView = new ModelAndView("shopping cart.html");
        modelAndView.addObject("cartIsEmpty", (loginHolder.getCurrentUserId() == 0)
                || ((loginHolder.getCurrentUserId() != 0) && (shoppingCartService.calculateOrderPrice(books) == 0)));
        modelAndView.addObject("books", books);
        modelAndView.addObject("totalItems", books.size());
        modelAndView.addObject("totalPrice", shoppingCartService.calculateOrderPrice(books));
        modelAndView.addObject("categories", BookService.categories);
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);

        return modelAndView;
    }

    @PostMapping("/delete_item")
    public String delete(@RequestParam("id") Long bookId) {
        shoppingCartService.deleteBookFromShoppingCart(bookId);
        return "redirect:/shopping_cart";
    }

    @PostMapping("/buy")
    public ModelAndView orderReview() throws Exception {
        ModelAndView modelAndView = new ModelAndView("order_review.html");
        modelAndView.addObject("books", shoppingCartService.order());
        return modelAndView;

    }
}