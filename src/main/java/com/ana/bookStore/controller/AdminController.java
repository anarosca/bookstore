package com.ana.bookStore.controller;

import com.ana.bookStore.service.AdminService;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.LoginHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class AdminController {

    @Autowired
    AdminService adminService;
    @Autowired
    BookService bookService;
    @Autowired
    LoginHolder loginHolder;

    @RequestMapping("/admin")
    public ModelAndView showAdminPage() throws Exception {
            Map<String, Long> monthlyEarnings = adminService.calculateEarningsPerMonth();
            Map<String, String> soldPercentagePerCategory = adminService.soldItemsPercentage();
            Map<String, Integer> availableItemsPerCategory = adminService.availableItemsPerCategory();

            Set<Book> mostRatedBooks = adminService.getMostRatedBooks();
            Set<Book> mostReviewedBooks = adminService.getMostReviewedBooks();

            ModelAndView modelAndView = new ModelAndView("admin.html");

            modelAndView.addObject("monthlyEarnings", monthlyEarnings);

            modelAndView.addObject("availableItemsPerCategory", availableItemsPerCategory);

            modelAndView.addObject("bestSeller", soldPercentagePerCategory.get("Best Seller"));
            modelAndView.addObject("childrenBooks", soldPercentagePerCategory.get("Children's Books"));
            modelAndView.addObject("computerAndTechnology", soldPercentagePerCategory.get("Computer and Technology"));
            modelAndView.addObject("history", soldPercentagePerCategory.get("history"));
            modelAndView.addObject("literatureAndFiction", soldPercentagePerCategory.get("Literature and Fiction"));
            modelAndView.addObject("medicalBooks", soldPercentagePerCategory.get("Medical Books"));
            modelAndView.addObject("romance", soldPercentagePerCategory.get("Romance"));

            modelAndView.addObject("earnings", adminService.getTotalEarnings());
            modelAndView.addObject("users", adminService.getUsers());
            modelAndView.addObject("sales", adminService.getSoldBooksAmount());
            modelAndView.addObject("reviews", adminService.geTotalReviews());

            modelAndView.addObject("mostRatedBooks", mostRatedBooks);
            modelAndView.addObject("mostReviewedBooks", mostReviewedBooks);
            return modelAndView;
    }

    @GetMapping("/addNewBooks")
    public ModelAndView addNewBooks() throws Exception {
            ModelAndView modelAndView = new ModelAndView("add_book.html");
            modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);
            modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));
            return modelAndView;
    }
    @PostMapping( value = "/addBooks")
    public String addBooks(HttpServletRequest request) throws Exception {
        adminService.addBooks(request);
        return "redirect:/addNewBooks";
    }
    @GetMapping("/addNewUsers")
    public ModelAndView addNewUsers() {
        ModelAndView modelAndView = new ModelAndView("add_user.html");
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);
        modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));
        return modelAndView;
    }
    @PostMapping("/addUsers")
    public String addUsers(HttpServletRequest request) throws Exception {
        adminService.addUsers(request);
        return "redirect:/addNewUsers";
    }
    @GetMapping("/modifyBooksPage")
    public ModelAndView modifyBooksPage() {
        ModelAndView modelAndView = new ModelAndView("modify_book.html");
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);
        modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));
        return modelAndView;
    }
    @PostMapping( value = "/modifyBooks")
    public String modifyBooks(HttpServletRequest request) throws Exception {
        adminService.modifyBooks(request);
        return "redirect:/modifyBooksPage";
    }
    @GetMapping("/modifyUsersPage")
    public ModelAndView modifyUsersPage() {
        ModelAndView modelAndView = new ModelAndView("modify_user.html");
        modelAndView.addObject("logged", loginHolder.getCurrentUserId() != 0);
        modelAndView.addObject("loggedAsAdmin", bookService.loggedAsAdmin(loginHolder.getCurrentUserId()));
        return modelAndView;
    }
    @PostMapping("/modifyUsers")
    public String modifyUsers(HttpServletRequest request) throws Exception {
        adminService.modifyUsers(request);
        return "redirect:/modifyUsersPage";
    }
    @PostMapping("/deleteBook")
    public String deleteBook(@RequestParam("id") Long id){
        bookService.deleteBookById(id);
        return "redirect:/admin";
    }
}
