package com.ana.bookStore;

import org.springframework.stereotype.Component;

@Component
public class CategoryURL {
    public static final String BEST_SELLER
            = "https://www.amazon.com/gp/bestsellers/books";
    public static final String CHILDREN_BOOKS
            = "https://www.amazon.com/Best-Sellers-Books-Childrens/zgbs/books/4/ref=zg_bs_nav_b_1_b";
    public static final String COMPUTER_AND_TECHNOLOGY
            = "https://www.amazon.com/Best-Sellers-Books-Computers-Technology/zgbs/books/5/ref=zg_bs_nav_b_1_b";
    public static final String HISTORY
            = "https://www.amazon.com/Best-Sellers-Books-History/zgbs/books/9/ref=zg_bs_nav_b_1_b";
    public static final String LITERATURE_AND_FICTION
            = "https://www.amazon.com/Best-Sellers-Books-Literature-Fiction/zgbs/books/17/ref=zg_bs_nav_b_1_b";
    public static final String MEDICAL_BOOKS
            = "https://www.amazon.com/Best-Sellers-Books-Medical/zgbs/books/173514/ref=zg_bs_nav_b_1_b";
    public static final String ROMANCE
            = "https://www.amazon.com/Best-Sellers-Books-Romance/zgbs/books/23/ref=zg_bs_nav_b_1_b";

}
