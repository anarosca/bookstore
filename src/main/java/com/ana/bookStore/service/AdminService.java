package com.ana.bookStore.service;

import com.ana.bookStore.*;
import com.ana.bookStore.controller.AdminController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.Review;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.ReviewRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Null;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AdminService {

    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    BookService bookService;

    Logger logger = LoggerFactory.getLogger(AdminService.class);


    public Map<String, String> soldItemsPercentage() throws Exception {
        Map<String, String> percentagePerCategory = new HashMap<>();
        percentagePerCategory.put("Best Seller", calculatePercentagePerCategory("Best Seller"));
        percentagePerCategory.put("Children's Books", calculatePercentagePerCategory("Children's Books"));
        percentagePerCategory.put("Computer and Technology", calculatePercentagePerCategory("Computer and Technology"));
        percentagePerCategory.put("history", calculatePercentagePerCategory("history"));
        percentagePerCategory.put("Literature and Fiction", calculatePercentagePerCategory("Literature and Fiction"));
        percentagePerCategory.put("Medical Books", calculatePercentagePerCategory("Medical Books"));
        percentagePerCategory.put("Romance", calculatePercentagePerCategory("Romance"));

        return percentagePerCategory;
    }
     public String calculatePercentagePerCategory(String category) throws Exception {
        try {
            Integer remainingStock = 0;
            Integer soldItemsNumber = 0;
            List<Book> books = bookRepository.findAllByCategory(category);
            for (Book book : books
            ) {
                remainingStock += book.getQuantity();
            }
            List<UserBook> orderedBooks = userBookRepository.findAllByActions(Actions.ORDERED);
            orderedBooks
                    .stream()
                    .filter(item -> item.getBook().getCategory().equals(category)); //filter books by category
            for (UserBook orderedItem : orderedBooks
            ) {
                soldItemsNumber += orderedItem.getBook().getQuantity();
            }
            return Integer.toString(soldItemsNumber * 100 / (soldItemsNumber + remainingStock)) + "%";
        } catch (Exception e) {
            logger.error("Something went wrong when calculating percentage per category: ",e);
            throw new Exception("It looks like the server did something wrong");        }
    }

    public Map<String, Long> calculateEarningsPerMonth() throws Exception {
        try {
            Map<String, Long> earningsPerMonth = new TreeMap<>(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return Month.valueOf(o1).compareTo(Month.valueOf(o2));
                }
            });
            Calendar calendar = Calendar.getInstance();
            int currentMonth = calendar.get(Calendar.MONTH);
            for (int month = 1; month <= currentMonth + 1; month++) {
                double earnings = 0;
                List<UserBook> soldItems = userBookRepository.findAllByActions(Actions.ORDERED);
                for (UserBook item : soldItems) {
                    if (item.getDate().getMonthValue() == month) {
                        earnings += item.getQuantity() * item.getBook().getPrice();
                    }
                }
                earningsPerMonth.put(Month.of(month).toString(), (long) earnings);
            }
            return earningsPerMonth;
        }catch (Exception e){
            logger.error("Something went wrong when calculating earining per month: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }

    public Map<String, Integer> availableItemsPerCategory() throws Exception {
        Map<String, Integer> itemsPerCategory = new HashMap<>();
        itemsPerCategory.put("Best Seller", getQuantityForCategory("Best Seller"));
        itemsPerCategory.put("Children's Books", getQuantityForCategory("Children's Books"));
        itemsPerCategory.put("Computer and Technology", getQuantityForCategory("Computer and Technology"));
        itemsPerCategory.put("History", getQuantityForCategory("History"));
        itemsPerCategory.put("Literature and Fiction", getQuantityForCategory("Literature and Fiction"));
        itemsPerCategory.put("Medical Books", getQuantityForCategory("Medical Books"));
        itemsPerCategory.put("Romance", getQuantityForCategory("Romance"));

        return itemsPerCategory;
    }
    public int getQuantityForCategory(String category) throws Exception {
        try {
            int quantity = 0;
            List<Book> books = bookRepository.findAllByCategory(category);
            for (Book book : books
            ) {
                quantity += book.getQuantity();
            }
            return quantity;
        } catch (Exception e) {
            logger.error("Something went wrong when fetching books from database: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }
    public Long getTotalEarnings() throws Exception {
        try {
            double totalAmount = 0.0;
            List<UserBook> orders = userBookRepository.findAllByActions(Actions.ORDERED);
            for (UserBook item : orders
            ) {
                totalAmount += item.getQuantity() * item.getBook().getPrice();
            }
            return (long) totalAmount;
        } catch (Exception e) {
            logger.error("Something went wrong when fetching orders from database: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }
    public Long getSoldBooksAmount() throws Exception {
        try {
            Long soldBooksAmount = 0L;
            List<UserBook> orders = userBookRepository.findAllByActions(Actions.ORDERED);
            for (UserBook item : orders
            ) {
                soldBooksAmount += item.getQuantity();
            }
            return (long) soldBooksAmount;
        } catch (Exception e) {
            logger.error("Something went wrong when fetching orders from database: ",e);
            throw new Exception("It looks like the server did something wrong");        }
    }
    public Long getUsers(){
        return (long)userRepository.findAll().size();
    }

    public Long geTotalReviews(){
        return (long)reviewRepository.findAll().size();
    }

    public Set<Book> getMostRatedBooks() throws Exception {
        try {
            Long bookRating;
            Map<Book, Long> bookRatings = new HashMap<Book, Long>();
            List<Book> books = bookRepository.findAll();
            for (Book book : books
            ) {
                bookRating = 0L;
                List<Review> reviews = reviewRepository.findAllByBookId(book.getId())
                        .stream()
                        .filter(item -> item.getBookRating() != null)
                        .collect(Collectors.toList());
                for (Review review : reviews
                ) {
                    bookRating += review.getBookRating().ordinal();
                }
                bookRatings.put(book, bookRating);
            }
            Map<Book, Long> sortedNewMap = bookRatings.entrySet().stream().sorted((e1, e2) ->
                    e2.getValue().compareTo(e1.getValue())).limit(4)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,    //sorting map by it's values
                            (e1, e2) -> e1, LinkedHashMap::new));

            return sortedNewMap.keySet();
        } catch (Exception e) {
            logger.error("Something went wrong when trying to retrieve most rated books: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }

    public Set<Book> getMostReviewedBooks() throws Exception {
        try {
            Long bookReviewsNumber;
            List<Book> mostReviewedBooks = new ArrayList<>();
            Map<Book, Long> bookReviews = new HashMap<Book, Long>();
            List<Book> books = bookRepository.findAll();
            for (Book book : books
            ) {
                bookReviewsNumber = 0L;
                List<Review> reviews = reviewRepository.findAllByBookId(book.getId())
                        .stream()
                        .filter(item -> item.getText() != null)
                        .collect(Collectors.toList());
                bookReviewsNumber += reviews.size();
                bookReviews.put(book, bookReviewsNumber);
            }

            Map<Book, Long> sortedNewMap = bookReviews.entrySet().stream().sorted((e1, e2) ->
                    e2.getValue().compareTo(e1.getValue())).limit(4)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,    //sorting map by it's values
                            (e1, e2) -> e1, LinkedHashMap::new));

            return sortedNewMap.keySet();
        } catch (Exception e) {
            logger.error("Something went wrong when trying to retrieve most reviewed books: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }
    public void addBooks(HttpServletRequest request) throws Exception {
        try {
            List<String> bookTitles = Arrays.asList(request.getParameterValues("book_title"));
            List<String> authors = Arrays.asList(request.getParameterValues("author"));
            List<String> descriptions = Arrays.asList(request.getParameterValues("description"));
            List<String> categories = Arrays.asList(request.getParameterValues("category"));
            List<Double> prices = Arrays.asList(request.getParameterValues("price"))
                    .stream()
                    .map(item -> Double.parseDouble(item))
                    .collect(Collectors.toList());
            List<Integer> quantities = Arrays.asList(request.getParameterValues("quantity"))
                    .stream()
                    .map(item -> Integer.parseInt(item))
                    .collect(Collectors.toList());

            if(bookTitles.contains("") || authors.contains("") || descriptions.contains("") || categories.contains("") || prices.contains("") || quantities.contains(""))
                throw new Exception("All fields are required for adding a new book!");

            List<Book> books = new ArrayList<>();

            Iterator bI = bookTitles.iterator();
            Iterator aI = authors.iterator();
            Iterator dI = descriptions.iterator();
            Iterator cI = categories.iterator();
            Iterator pI = prices.iterator();
            Iterator qI = quantities.iterator();

            while (bI.hasNext()
                    && aI.hasNext()
                    && cI.hasNext()
                    && dI.hasNext()
                    && pI.hasNext()
                    && qI.hasNext()) {
                books.add(new Book((String) bI.next(),
                        (String) aI.next(),
                        (String) dI.next(),
                        (String) cI.next(),
                        (Double) pI.next(),
                        (Integer) qI.next()));
            }
            bookRepository.saveAll(books);
        }catch(NumberFormatException e){
        logger.error("Something went wrong when parsing values from the user: ",e);
        throw new NumberFormatException("The price and quantity must be numbers!");
    }
    }

    public void addUsers(HttpServletRequest request) throws Exception {
            List<String> usernames = Arrays.asList(request.getParameterValues("username"));
            List<String> emails = Arrays.asList(request.getParameterValues("email"));
            List<String> passwords = Arrays.asList(request.getParameterValues("password"));
            List<String> confirmedPasswords = Arrays.asList(request.getParameterValues("confirm-password"));

            if(usernames.contains("") || emails.contains("") || passwords.contains("") || confirmedPasswords.contains(""))
                throw new Exception("All fields are required for adding a new user!");
            List<User> users = new ArrayList<>();

            Iterator uI = usernames.iterator();
            Iterator eI = emails.iterator();
            Iterator pI = passwords.iterator();
            Iterator cpI = confirmedPasswords.iterator();


            while (uI.hasNext() && eI.hasNext() && pI.hasNext() && cpI.hasNext()) {
                String password = (String) pI.next();
                String confirmPassword = (String) cpI.next();
                if (!password.equals(confirmPassword))
                    throw new Exception("Passwords doesn't match!");
                users.add(new User((String) uI.next(), (String) eI.next(), password));
            }
            userRepository.saveAll(users);
    }

    public void modifyUsers(HttpServletRequest request) throws Exception {
        List<String> oldUsernames = Arrays.asList(request.getParameterValues("oldUsername"));
        List<String> usernames = Arrays.asList(request.getParameterValues("username"));
        List<String> emails = Arrays.asList(request.getParameterValues("email"));
        List<String> passwords = Arrays.asList(request.getParameterValues("password"));
        List<String> confirmedPasswords = Arrays.asList(request.getParameterValues("confirm-password"));

        if(oldUsernames.contains("") || usernames.contains("") || emails.contains("") || passwords.contains("") || confirmedPasswords.contains(""))
            throw new Exception("All fields are required to modify an existing user!");

        Iterator oldUI = oldUsernames.iterator();
        Iterator uI = usernames.iterator();
        Iterator eI = emails.iterator();
        Iterator pI = passwords.iterator();
        Iterator cpI = confirmedPasswords.iterator();


        while( oldUI.hasNext() && uI.hasNext() && eI.hasNext() && pI.hasNext()) {
            String oldUsername = (String) oldUI.next();
            String password = (String) pI.next();
            String confirmedPassword = (String) cpI.next();
            if (!password.equals(confirmedPassword))
                throw new Exception("Passwords doesn't match!");

                User user = userRepository.findByUsername(oldUsername);
                if(user == null) {
                    throw new NullPointerException("The username that you entered does not exist! Please enter a valid one");
                }
                user.setUsername((String) uI.next());
                user.setEmailAddress((String) eI.next());
                user.setPassword(password);

               userRepository.save(user);
            }
    }

    public void modifyBooks(HttpServletRequest request) throws Exception {
        try {
            List<String> bookTitles = Arrays.asList(request.getParameterValues("book_title"));
            List<String> authors = Arrays.asList(request.getParameterValues("author"));
            List<String> descriptions = Arrays.asList(request.getParameterValues("description"));
            List<String> categories = Arrays.asList(request.getParameterValues("category"));
            List<Double> prices = Arrays.asList(request.getParameterValues("price"))
                    .stream()
                    .map(item -> Double.parseDouble(item))
                    .collect(Collectors.toList());
            List<Integer> quantities = Arrays.asList(request.getParameterValues("quantity"))
                    .stream()
                    .map(item -> Integer.parseInt(item))
                    .collect(Collectors.toList());
            if(bookTitles.contains("") || authors.contains("") || descriptions.contains("") || categories.contains("") || prices.contains("") || quantities.contains(""))
                throw new Exception("All fields are required to modify an existing book!");

            Iterator bI = bookTitles.iterator();
            Iterator aI = authors.iterator();
            Iterator dI = descriptions.iterator();
            Iterator cI = categories.iterator();
            Iterator pI = prices.iterator();
            Iterator qI = quantities.iterator();

            while (bI.hasNext()
                    && aI.hasNext()
                    && cI.hasNext()
                    && dI.hasNext()
                    && pI.hasNext()
                    && qI.hasNext()) {

                Optional<Book> book = bookService.findByBookTitleIgnoreCase((String) bI.next());
                if (!book.isPresent()) {
                    logger.error("Title of the book to be modified does not exist");
                    throw new Exception("Title of the book you want to modify does not exist! Please enter a valid one");
                }
                book.get().setAuthor((String) aI.next());
                book.get().setCategory((String) cI.next());
                book.get().setDescription((String) dI.next());
                book.get().setPrice((Double) pI.next());
                book.get().setQuantity((Integer) qI.next());
                bookRepository.deleteById(1379L);
                bookRepository.deleteById(1380L);
                bookRepository.deleteById(1381L);
                bookRepository.deleteById(1382L);
                bookRepository.deleteById(1383L);
                bookRepository.deleteById(1384L);
                bookRepository.deleteById(1385L);
                bookRepository.deleteById(1386L);

                bookRepository.save(book.get());
                if(!BookService.categories.contains(book.get().getCategory())){
                    BookService.categories.add(book.get().getCategory());
                }
            }
        } catch (NumberFormatException e) {
            logger.error("Something went wrong when parsing values from the user: ", e);
            throw new NumberFormatException("The price and quantity must be numbers!");
        }
    }
}
