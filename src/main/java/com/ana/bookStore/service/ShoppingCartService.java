package com.ana.bookStore.service;

import com.ana.bookStore.Actions;
import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShoppingCartService {

    @Autowired
    UserBookRepository userBookRepository;
    @Autowired
    BookRepository bookRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    LoginHolder loginHolder;

    Logger logger = LoggerFactory.getLogger(ShoppingCartService.class);

    public void addToCart(Long userId, Long bookId) throws Exception {
        Book book = bookRepository.findById(bookId).get();
        User user = userRepository.findById(userId).get();
        if (book.getQuantity() == 0) {
            throw new Exception(book.getBookTitle() + " is sold out! Please keep in touch for further updates");
        }
        UserBook cart = new UserBook(user, book);

        cart.setActions(Actions.ADD_TO_SHOPPING_CART);
        user.getBooks().add(cart);
        book.getUsers().add(cart);

        userBookRepository.save(cart);
    }
    public List<Book> shoppingCartList(Long userId){
        List<UserBook> shoppingCartItems = userBookRepository.findAllByUserAndActions(
                userRepository.findById(userId).get(),
                Actions.ADD_TO_SHOPPING_CART);

        List<Book> books = shoppingCartItems
                .stream()
                .map(item -> item.getBook())
                .collect(Collectors.toList());
        return books;
    }
    public double calculateOrderPrice(List<Book> books){
        Double totalPrice = 0.0;
        for(Book book: books){
            totalPrice += book.getPrice();
        }
        Double truncatedDoublePrice = BigDecimal.valueOf(totalPrice)
                .setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
        return truncatedDoublePrice;
    }
    public void deleteBookFromShoppingCart(Long bookId){
        userBookRepository.deleteByUserAndBookAndActions(
                userRepository.findById(loginHolder.getCurrentUserId()).get(),
                bookRepository.findById(bookId).get(),
                Actions.ADD_TO_SHOPPING_CART
        );
    }
    public void updateQuantity(Book orderedItem) {
            orderedItem.setQuantity(orderedItem.getQuantity() - 1);
            bookRepository.save(orderedItem);
    }
    public List<Book> order() throws Exception {
        try {
            List<UserBook> shoppingCartItems = userBookRepository.findAllByUserAndActions(
                    userRepository.findById(loginHolder.getCurrentUserId()).get(),
                    Actions.ADD_TO_SHOPPING_CART);

            for (UserBook item : shoppingCartItems) {
                item.setActions(Actions.ORDERED);
                item.setDate(LocalDate.now());
                item.setQuantity(18);
                updateQuantity(item.getBook());
                userBookRepository.save(item);
            }
            List<Book> order = shoppingCartItems
                    .stream()
                    .map(item -> item.getBook())
                    .collect(Collectors.toList());
            return order;
        } catch (Exception e) {
            logger.error("Something went wrong when trying to retrieve ordered items for user: ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }
}
