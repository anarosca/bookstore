package com.ana.bookStore.service;

import com.ana.bookStore.*;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.Review;
import com.ana.bookStore.model.User;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.ReviewRepository;
import com.ana.bookStore.repository.UserRepository;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookService {


    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private LoginHolder loginHolder;

    Logger logger = LoggerFactory.getLogger(BookService.class);

    public static List<String> categories = Arrays.asList("Best Seller", "Children's Books", "Computer and Technology",
            "History", "Literature and Fiction", "Medical Books", "Romance");


    public static List<Book> books = new ArrayList<>();

    public void getBooksInfo() throws IOException, InterruptedException {

        List<Book> books = new ArrayList<>();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        WebDriver webDriver = new ChromeDriver();
        try {
            saveBooksInfoByCategory(webDriver, CategoryURL.BEST_SELLER, "Best Seller");
            saveBooksInfoByCategory(webDriver, CategoryURL.CHILDREN_BOOKS, "Children's Books");
            saveBooksInfoByCategory(webDriver, CategoryURL.COMPUTER_AND_TECHNOLOGY, "Computer and Technology");
            saveBooksInfoByCategory(webDriver, CategoryURL.HISTORY, "History");
            saveBooksInfoByCategory(webDriver, CategoryURL.LITERATURE_AND_FICTION, "Literature and Fiction");
            saveBooksInfoByCategory(webDriver, CategoryURL.MEDICAL_BOOKS, "Medical Books");
            saveBooksInfoByCategory(webDriver, CategoryURL.ROMANCE, "Romance");
        }catch(Exception e){
            System.out.println(e);
        }
        finally {
            webDriver.close();
            webDriver.quit();
        }

    }

    private void saveBooksInfoByCategory(WebDriver webDriver, String url, String category){
        webDriver.get(url);
        try {
            for (int i = 1; i <= 40; i++) {
                String author;
                try{
                    author = findElementInPage(webDriver,
                            "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/div[1]/a")
                            .getText();
                }catch(Exception e){
                    author = findElementInPage(webDriver,
                            "//*[@id=\"zg-ordered-list\"]/li[" + i +"]/span/div/span/div[1]/span")
                            .getText();
                }
                String bookTitle;
                try {
                    bookTitle = findElementInPage(webDriver,
                            "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a[1]/div")
                            .getText();
                    if(bookTitle.charAt(bookTitle.length() -1) == 8230){
                        bookTitle = findElementInPage(webDriver,
                                "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a[1]/div")
                                .getAttribute("title");
                    }
                }catch(Exception e){
                    bookTitle = findElementInPage(webDriver,
                            "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a/div")
                            .getText();
                    if(bookTitle.charAt(bookTitle.length() -1) == 8230){
                        bookTitle = findElementInPage(webDriver,
                                "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a/div")
                                .getAttribute("title");
                    }
                }
                Double price;
                try{
                    price = Double.parseDouble(findElementInPage(webDriver,
                            "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/div[4]/a/span/span")
                            .getText().substring(1));
                }catch(Exception e){
                    try {
                        price = Double.parseDouble(findElementInPage(webDriver,
                                "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a[2]/span/span/span")
                                .getText().substring(1));
                    }catch(Exception ex){
                        try {
                            price = Double.parseDouble(findElementInPage(webDriver,
                                    "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/div[3]/a/span/span")
                                    .getText().substring(1));
                        }catch(Exception exception){
                            try {
                                price = Double.parseDouble(findElementInPage(webDriver,
                                        " //*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/div[4]/a/span")
                                        .getText().substring(1, 5));
                            }catch(Exception exx){
                                price = 0.00;
                            }
                        }
                    }
                }
                WebElement bookImage = findElementInPage(webDriver,
                        "//*[@id=\"zg-ordered-list\"]/li[" + i + "]/span/div/span/a[1]/span/div/img");
                File outputFile = saveImageAsFile(bookImage, bookTitle);

                Book book = new Book();
                book.setBookTitle(bookTitle);
                book.setPhoto("book images/" + outputFile.getName());
                book.setPrice(price);
                book.setAuthor(author);
                book.setCategory(category);
                books.add(book);
            }
        }catch (Exception e){
            logger.error("Something went wrong when fetching data from resource: ", e);
        }
        finally {
            bookRepository.saveAll(books);
            webDriver.close();
            webDriver.quit();}
    }

    private WebElement findElementInPage(WebDriver webDriver, String xpath){
        return webDriver.findElement
                (By.xpath(xpath));
    }

    private File saveImageAsFile(WebElement webElement, String bookTitle) throws IOException {
        String src = webElement.getAttribute("src");
        BufferedImage bufferedImage = ImageIO.read(new URL(src));
        String[] words = bookTitle.split("[ :;'?|*=()!\\[\\]-]+");   //any character of: ' ', ':', ';', ''', '?'
        File outputFile = new File("C:\\Users\\User\\Desktop\\bookStore\\src\\main\\resources\\static\\book images\\" +
                (words.length > 2 ? words[0] + words[1] + words[2] : (words.length > 1 ? words[0]+words[1] : words[0]))
                + ".jpg");
        ImageIO.write(bufferedImage,"jpg", outputFile);
        return outputFile;
    }

    public Page<Book> findPaginated(Pageable pageable, List<Book> books) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Book> list;

        if (books.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, books.size());
            list = books.subList(startItem, toIndex);
        }
        Page<Book> bookPage
                = new PageImpl<Book>(list, PageRequest.of(currentPage, pageSize), books.size());
        return bookPage;
    }

    public List<Book> getRelatedSearchItems(List<Book> bookList){
        int index = getRandomNumberInRange(5, bookList.size() -6);
        return bookList.subList(index, index + 4);
    }
    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public BookRating getBookRating(String rating){
        switch (rating){
            case "very-bad":
                return BookRating.VERY_BAD;
            case "poor":
                return BookRating.POOR;
            case "mediocre":
                return BookRating.MEDIOCRE;
            case "good":
                return BookRating.GOOD;
            case "excellent":
                return BookRating.EXCELLENT;
            default:
                return BookRating.MEDIOCRE;
        }
    }
    public String convertDateIntoString(LocalDate date){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy");
        String stringDate = dtf.format(date);
        return stringDate;
    }

    public boolean loggedAsAdmin(Long id){
        Optional<User> user = userRepository.findById(loginHolder.getCurrentUserId());
        if(!user.isPresent()){
            return false;
        }
        return (user.get().getUsername().equals("admin"));
    }

    public void addBookRating(String name, Long bookId, Long userId, String rating) throws Exception {
        try {
            Book book = bookRepository.findById(bookId).get();
            Optional<Review> existentReview = reviewRepository.findByUserIdAndBookId(userId, bookId);
            if (existentReview.isPresent()){
                existentReview.get().setUserId(userId);
                existentReview.get().setAuthorName(name);
                existentReview.get().setBook(book);
                existentReview.get().setBookRating(getBookRating(rating));

                book.getReviews().add(existentReview.get());
                reviewRepository.save(existentReview.get());
            }else {
                Review review = new Review();
                review.setUserId(userId);
                review.setAuthorName(name);
                review.setBook(book);
                review.setBookRating(getBookRating(rating));

                book.getReviews().add(review);
                reviewRepository.save(review);
            }
        }catch (Exception e){
            logger.error("Something went wrong when trying to rate book : ",e);
            throw new Exception("It looks like the server did something wrong");
        }
    }

    public void addBookReview(Long bookId, String text) throws Exception {
        try {
            User user = userRepository.findById(loginHolder.getCurrentUserId()).get();
            Book book = bookRepository.findById(bookId).get();
            Optional<Review> existentReview = reviewRepository.findByUserIdAndBookId(user.getId(), bookId);
            Review newReview = new Review();

            if (existentReview.isPresent() && existentReview.get().getText() == null) {
                newReview.setAuthorName(existentReview.get().getAuthorName());
                newReview.setBookRating(existentReview.get().getBookRating());

                reviewRepository.delete(existentReview.get());
            } else {
                newReview.setAuthorName(user.getUsername());
            }
            newReview.setBook(book);
            newReview.setUserId(user.getId());
            newReview.setText(text);
            newReview.setDate(convertDateIntoString(LocalDate.now()));

            book.getReviews().add(newReview);
            reviewRepository.save(newReview);
        } catch (Exception e) {
            logger.error("Something went wrong when trying to add book review: ", e);
            throw new Exception("It looks like the server did something wrong");
        }
    }
    public Map<String, String> calculateRatingsPerBook(Long bookId) throws Exception {
        try {
            Map<String, String> ratings = new HashMap<>();
            Integer totalRatingsNumber = 0;
            Integer poorRating = 0;
            Integer mediocreRating = 0;
            Integer goodRating = 0;
            Integer excellentRating = 0;

            List<Review> reviewsContainingBookRatings = reviewRepository.findAllByBookId(bookId)
                    .stream()
                    .filter(review -> review.getBookRating() != null)
                    .collect(Collectors.toList());
            if (reviewsContainingBookRatings.size() != 0) {
                for (Review review : reviewsContainingBookRatings) {
                    totalRatingsNumber += review.getBookRating().ordinal();
                    if (review.getBookRating().ordinal() == 1) {
                        poorRating += review.getBookRating().ordinal();
                    } else if (review.getBookRating().ordinal() == 2) {
                        mediocreRating += review.getBookRating().ordinal();
                    } else if (review.getBookRating().ordinal() == 3) {
                        goodRating += review.getBookRating().ordinal();
                    } else {
                        excellentRating += review.getBookRating().ordinal();
                    }
                }
            }
            ratings.put("Total Ratings", Integer.toString(reviewsContainingBookRatings.size()));
            ratings.put("Excellent Rating", totalRatingsNumber > 0 ? Integer.toString(excellentRating * 100 / totalRatingsNumber) + "%" : "0%");
            ratings.put("Good Rating", totalRatingsNumber > 0 ? Integer.toString(goodRating * 100 / totalRatingsNumber) + "%" : "0%");
            ratings.put("Mediocre Rating", totalRatingsNumber > 0 ? Integer.toString(mediocreRating * 100 / totalRatingsNumber) + "%" : "0%");
            ratings.put("Poor Rating", totalRatingsNumber > 0 ? Integer.toString(poorRating * 100 / totalRatingsNumber) + "%" : "0%");

            return ratings;
        }catch(Exception e){
            logger.error("Something went wrong when trying to calculate book rating : ", e);
            throw new Exception("It looks like the server did something wrong");
        }
    }

    public List<Book> findAllByCategory(String category) {
        return bookRepository.findAllByCategory(category);
    }

    public Optional<Book> findById(Long bookId) {
        return bookRepository.findById(bookId);
    }

    public Optional<Book> findByBookTitleIgnoreCase(String bookToSearch) {
        return bookRepository.findByBookTitleIgnoreCase(bookToSearch).stream().findFirst();
    }
    public void deleteBookById(Long bookId){
        reviewRepository.deleteAllByBookId(bookId);
        bookRepository.deleteById(bookId);
    }
}