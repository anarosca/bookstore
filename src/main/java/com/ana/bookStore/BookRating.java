package com.ana.bookStore;

public enum BookRating {
    VERY_BAD,
    POOR,
    MEDIOCRE,
    GOOD,
    EXCELLENT
}
