package com.ana.bookStore.repository;

import com.ana.bookStore.Actions;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserBookRepository extends JpaRepository<UserBook, Long> {

    List<UserBook> findAllByUserAndActions(User user, Actions actions);

    List<UserBook> findAllByActions(Actions actions);
    @Transactional
    void deleteByUserAndBookAndActions(User user, Book book, Actions actions);
}
