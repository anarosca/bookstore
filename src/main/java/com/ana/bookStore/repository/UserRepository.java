package com.ana.bookStore.repository;

import com.ana.bookStore.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    public User save(User user);

    public User findByUsername(String username);

    public User findByEmailAddress(String emailAddress);

    public Optional<User> findById(Long id);
}

