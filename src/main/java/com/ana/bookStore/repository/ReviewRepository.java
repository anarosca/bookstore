package com.ana.bookStore.repository;

import com.ana.bookStore.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long> {

    Optional<Review> findByUserIdAndBookId(Long userId, Long bookId);

    Optional<Review> findByAuthorName(String author);

    @Transactional
    void deleteByUserIdAndBookId(Long userId, Long bookId);

    @Transactional
    void deleteAllByBookId(Long bookId);

    List<Review> findAllByBookId(Long bookId);
}
