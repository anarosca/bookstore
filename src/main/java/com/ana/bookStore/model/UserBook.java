package com.ana.bookStore.model;

import com.ana.bookStore.Actions;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity(name = "UserBook")
@Table(name = "user_book")
public class UserBook {


    @EmbeddedId
    UserBookId userBookId;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("bookId")
    private Book book;

    @Column(name = "action_type")
    private Actions actions;

    private LocalDate date;

    private int quantity;

    public UserBook() {}

    public UserBook(User user, Book book){
        this.user = user;
        this.book = book;
        this.userBookId = new UserBookId(user.getId(), book.getId());
    }

    public UserBookId getUserBookId() {
        return userBookId;
    }

    public void setUserBookId(UserBookId userBookId) {
        this.userBookId = userBookId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Actions getActions() {
        return actions;
    }

    public void setActions(Actions actions) {
        this.actions = actions;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        UserBook that = (UserBook) obj;
        return this.user.equals(that.user) &&
                this.book.equals(that.book);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book);
    }

}
