package com.ana.bookStore.service;


import com.ana.bookStore.Actions;
import com.ana.bookStore.controller.ShoppingCartController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.ReviewRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.AdminService;
import com.ana.bookStore.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class AdminServiceTest {

    @Mock
    UserBookRepository userBookRepository;
    @Mock
    BookRepository bookRepository;
    @Mock
    UserRepository userRepository;
    @Mock
    ReviewRepository reviewRepository;
    @Mock
    BookService bookService;
    @InjectMocks
    AdminService adminService;

    @Test
    public void testCalculatePercentagePerCategory() throws Exception {
        List<Book> booksByCategory = Arrays.asList(new Book(), new Book());
        List<UserBook> orderedBooks = Arrays.asList(new UserBook(new User(),new Book()), new UserBook(new User(), new Book()));
        booksByCategory.forEach(book -> book.setQuantity(1));
        orderedBooks.forEach(orderedBook -> {
            orderedBook.getBook().setCategory("category");
            orderedBook.getBook().setQuantity(1);
        });
        when(bookRepository.findAllByCategory(any(String.class))).thenReturn(booksByCategory);
        when(userBookRepository.findAllByActions(Actions.ORDERED)).thenReturn(orderedBooks);
        String percentage = adminService. calculatePercentagePerCategory("category");
        assertThat(percentage).isEqualTo("50%");
    }

    @Test
    public void testCalculateEarningPerMonth() throws Exception {
        List<UserBook> orderedItems = Arrays.asList(new UserBook(new User(),new Book()), new UserBook(new User(), new Book()));
        orderedItems.forEach(item -> {
            item.setDate(LocalDate.now());
            item.setQuantity(1);
            item.getBook().setPrice(10);
        });
        when(userBookRepository.findAllByActions(Actions.ORDERED)).thenReturn(orderedItems);
        Map<String, Long> earningsPerMonth = adminService.calculateEarningsPerMonth();
        assertThat(earningsPerMonth.get("JUNE")).isEqualTo(20);
    }

    @Test
    public void testGetQuantityForCategory() throws Exception {
        List<Book> booksByCategory = Arrays.asList(new Book(), new Book());
        booksByCategory.forEach(book -> book.setQuantity(50));
        when(bookRepository.findAllByCategory(any(String.class))).thenReturn(booksByCategory);
        int quantity = adminService.getQuantityForCategory("category");
        assertThat(quantity).isEqualTo(100);
    }

    @Test
    public void testGetTotalEarnings() throws Exception {
        List<UserBook> orderedBooks = Arrays.asList(new UserBook(new User(),new Book()), new UserBook(new User(), new Book()), new UserBook(new User(),new Book()), new UserBook(new User(), new Book()));
        orderedBooks.forEach(item -> {
            item.setQuantity(10);
            item.getBook().setPrice(10);
        });
        when(userBookRepository.findAllByActions(any(Actions.class))).thenReturn(orderedBooks);
        Long earnings = adminService.getTotalEarnings();
        assertThat(earnings).isEqualTo(400);
    }
}
