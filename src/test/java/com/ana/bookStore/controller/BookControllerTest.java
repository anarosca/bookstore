package com.ana.bookStore.controller;

import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.controller.BookController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.Review;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.ReviewRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(JUnit4.class)
@WebMvcTest(BookController.class)
public class BookControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private LoginHolder loginHolder;
    @MockBean
    private BookService bookService;
    @MockBean
    private BookRepository bookRepository;
    @MockBean
    private ReviewRepository reviewRepository;
    @MockBean
    private UserRepository userRepository;


    @Test
    public void viewBookTest() throws Exception {
        Long bookTestId = 1L;
        Optional<Book> book = Optional.of(new Book("Test", "Category"));
        List<Book> booksByCategory = Arrays.asList(new Book("Book Title", "Category"));
        List<Book> relatedSearchItems = Arrays.asList(new Book("RelatedSearchItem", "Category"));
        List<Review> bookReviews = Collections.emptyList();
        Map<String, String> ratings = Collections.singletonMap("Total Ratings", "100");

        when(bookService.findById(bookTestId)).thenReturn(book);
        when(bookService.findAllByCategory(book.get().getCategory())).thenReturn(booksByCategory);
        when(bookService.getRelatedSearchItems(booksByCategory)).thenReturn(relatedSearchItems);
        when(reviewRepository.findAllByBookId(any(Long.class))
                .stream()
                .filter(review -> review.getDate() != null)
                .collect(Collectors.toList())).thenReturn(bookReviews);
        when(bookService.calculateRatingsPerBook(bookTestId)).thenReturn(ratings);
        when(bookService.loggedAsAdmin(any(Long.class))).thenReturn(false);
        when(loginHolder.getCurrentUserId()).thenReturn(100L);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/book/").param("book_id", String.valueOf(bookTestId)))
                .andExpect(model().size(11))
                .andExpect(model().attribute("totalRatings", ratings.get("Total Ratings")))
                .andExpect(model().attribute("book", book.get()))
                .andExpect(model().attribute("relatedSearchItems", relatedSearchItems))
                .andExpect(model().attribute("reviews", bookReviews))
                .andExpect(status().isOk());

    }
    @Test
    public void searchExistingBook() throws Exception {
        Optional<Book> book = Optional.of(new Book(1L, "Test"));
        when(bookService.findByBookTitleIgnoreCase(book.get().getBookTitle())).thenReturn(book);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/search").param("search", String.valueOf(book.get().getBookTitle())))
                .andExpect(redirectedUrl("/book?book_id=" + book.get().getId())).andExpect(status().isFound());

    }
    @Test(expected = Exception.class)
    public void searchNonExistingBookThrowsException() throws Exception {
        when(bookService.findByBookTitleIgnoreCase("Title")).thenReturn(null);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/search").param("search", "Title"));


    }
}
