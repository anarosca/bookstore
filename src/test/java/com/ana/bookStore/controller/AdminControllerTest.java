package com.ana.bookStore.controller;

import com.ana.bookStore.controller.AdminController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.service.AdminService;
import com.ana.bookStore.service.BookService;
import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@RunWith(SpringRunner.class)
@WebMvcTest(AdminController.class)
public class AdminControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AdminService adminService;

    @Test
    public void showAdminPageShouldReturnAllObjectsInView() throws Exception {
        Map<String, Long> monthlyEarnings = Collections.singletonMap("July",1000L);
        Map<String, String> soldPercentagePerCategory = Collections.singletonMap("Best Seller","100%");
        Map<String, Integer> availableItemsPerCategory = Collections.singletonMap("Romance",1000);
        Set<Book> mostRatedBooks = Collections.singleton(new Book("MostRated"));
        Set<Book> mostReviewedBooks = Collections.singleton(new Book("MostReviewed"));
        Long totalEarnings = 1_000_000L;
        Long users = 1_000L;
        Long soldBooksAmount = 10_000L;
        Long totalReviews = 100_000L;

        when(adminService.calculateEarningsPerMonth()).thenReturn(monthlyEarnings);
        when(adminService.soldItemsPercentage()).thenReturn(soldPercentagePerCategory);
        when(adminService.availableItemsPerCategory()).thenReturn(availableItemsPerCategory);
        when(adminService.getMostRatedBooks()).thenReturn(mostRatedBooks);
        when(adminService.getMostReviewedBooks()).thenReturn(mostReviewedBooks);
        when(adminService.getTotalEarnings()).thenReturn(1000000L);
        when(adminService.getUsers()).thenReturn(1000L);
        when(adminService.getSoldBooksAmount()).thenReturn(10000L);
        when(adminService.geTotalReviews()).thenReturn(100000L);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/admin"))
                .andExpect(model().size(15))
                .andExpect(model().attribute("availableItemsPerCategory", availableItemsPerCategory))
                .andExpect(model().attribute("bestSeller", soldPercentagePerCategory.get("Best Seller")))
                .andExpect(model().attribute("earnings", totalEarnings))
                .andExpect(model().attribute("users", users))
                .andExpect(model().attribute("sales", soldBooksAmount))
                .andExpect(model().attribute("reviews", totalReviews))
                .andExpect(model().attribute("mostRatedBooks", mostRatedBooks))
                .andExpect(model().attribute("mostReviewedBooks", mostReviewedBooks))
                .andExpect(status().isOk());
    }

}
