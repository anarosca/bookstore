package com.ana.bookStore.controller;

import com.ana.bookStore.Actions;
import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.controller.BookController;
import com.ana.bookStore.controller.FavoritesController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.model.User;
import com.ana.bookStore.model.UserBook;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.service.UserService;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@WebMvcTest(FavoritesController.class)
public class FavoritesControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    UserBookRepository userBookRepository;
    @MockBean
    BookService bookService;
    @MockBean
    UserService userService;
    @MockBean
    LoginHolder loginHolder;


    @Test
    public void addToFavoritesShouldRedirectToFavoritesWhenUserIsLoggedIn() throws Exception {
        Optional<Book> book = Optional.of(new Book());
        Optional<User> user = Optional.of(new User());
        UserBook userBook = new UserBook();
        when(loginHolder.getCurrentUserId()).thenReturn(1L);
        when(userBookRepository.findAllByUserAndActions(
                any(User.class),
                any(Actions.class))).thenReturn(Collections.emptyList());
        when(userService.findById(loginHolder.getCurrentUserId())).thenReturn(user);
        when(bookService.findById(any(Long.class))).thenReturn(book);
        when(userBookRepository.save(new UserBook(user.get(), book.get()))).thenReturn(userBook);

        mockMvc.perform(MockMvcRequestBuilders
                .get("/addToFavorites")
                .param("id", String.valueOf(1L)))
                .andExpect(redirectedUrl("/favorites")).andExpect(status().isFound());
    }
    @Test
    public void addToFavoritesShouldRedirectToSignInWhenUserIsNotLoggedIn() throws Exception {
        when(loginHolder.getCurrentUserId()).thenReturn(0L);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/addToFavorites")
                .param("id", String.valueOf(1L)))
                .andExpect(redirectedUrl("/signin.html")).andExpect(status().isFound());
    }

    @Test
    public void favoritesShouldReturnAllObjectsInView() throws Exception {
        List<UserBook> favorites = Arrays.asList(new UserBook());
        when(loginHolder.getCurrentUserId()).thenReturn(1L);
        when(userBookRepository.findAllByUserAndActions(
                any(User.class),
                any(Actions.class))).thenReturn(favorites);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/favorites"))
                .andExpect(model().size(1))
                .andExpect(status().isOk());

    }
    @Test
    public void favoritesShouldRedirectUserToSignInWhenNotLoggedIn() throws Exception {
        when(loginHolder.getCurrentUserId()).thenReturn(0L);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/favorites"))
                .andExpect(redirectedUrl("/signin.html")).andExpect(status().isFound());
    }
}
