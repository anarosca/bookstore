package com.ana.bookStore.controller;

import com.ana.bookStore.LoginHolder;
import com.ana.bookStore.controller.FavoritesController;
import com.ana.bookStore.controller.ShoppingCartController;
import com.ana.bookStore.model.Book;
import com.ana.bookStore.repository.BookRepository;
import com.ana.bookStore.repository.UserBookRepository;
import com.ana.bookStore.repository.UserRepository;
import com.ana.bookStore.service.BookService;
import com.ana.bookStore.service.ShoppingCartService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ShoppingCartController.class)
public class ShoppingCartControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    LoginHolder loginHolder;
    @MockBean
    ShoppingCartService shoppingCartService;
    @MockBean
    UserBookRepository userBookRepository;
    @MockBean
    BookRepository bookRepository;
    @MockBean
    UserRepository userRepository;


    @Test
    public void shoppingCartShouldReturnAllObjectsInView() throws Exception {
        List<Book> books = Arrays.asList(new Book());
        when(loginHolder.getCurrentUserId()).thenReturn(1L);
        when(shoppingCartService.shoppingCartList(any(Long.class))).thenReturn(books);
        when(shoppingCartService.calculateOrderPrice(books)).thenReturn(1.00);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/shopping_cart"))
        .andExpect(model().size(6)).andExpect(status().isOk());
    }

    @Test
    public void shoppingCartShouldRedirectUserToSignInWhenNotLoggedIn() throws Exception {
        when(loginHolder.getCurrentUserId()).thenReturn(0L);
        mockMvc.perform(MockMvcRequestBuilders
                .get("/shopping_cart"))
                .andExpect(redirectedUrl("/signin.html")).andExpect(status().isFound());
    }
}

